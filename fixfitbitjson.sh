#!/bin/bash

if [ $# -ne 3 ]
then
  echo "Usage: $0 <database> <username> <password> ..."
  exit 1
fi

db=$1
user=$2
password=$3

echo "Starting to process JSON files in ${PWD} into database ${db} as user ${user}"

for file in *.json
do
    echo "Processing: ${file}"

    # "logId" -> "_id"
    sed -i 's/"logId"/"_id"/g' ${file}

    # "12345" -> 12345
    sed -i -r 's|\"([0-9]+)\"|\1|g' ${file}
    # "12345.678" -> 12345.678
    sed -i -r 's|\"([0-9]+\.[0-9]+)\"|\1|g' ${file}
    
    #"07/31/18 00:00:00" -> "2018-07-31T00:00:00.000"
    sed -i -r 's|\"([0-9]{2})/([0-9]{2})/([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})\"|\"20\3-\1-\2T\4:\5:\6.000\"|g' ${file}
    
    #"07/31/18" -> "2018-07-31T00:00:00.000"
    sed -i -r 's|\"([0-9]{2})/([0-9]{2})/([0-9]{2})"|\"20\3-\1-\2T00:00:00.000"|g' ${file}
    
    #"2018-07-31" -> "2018-07-31T00:00:00.000"
    sed -i -r 's|\"([0-9]{4})-([0-9]{2})-([0-9]{2})"|\"\1-\2-\3T00:00:00.000"|g' ${file}
    
    #"2018-08-30T00:29:00.000" -> {"$date" : "2018-08-30T00:29:00.000Z"}
    sed -i 's/\"[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}T[0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}.000\"/{\"$date\" : &\}/g' ${file}
    sed -i 's/.000"/.000Z"/g' ${file}
 
    collection=${file%%-*}
    echo "Loading Data into MongoDB Collection: ${collection}"

    mongoimport --db ${db} --collection ${collection} --file ${file} --jsonArray --authenticationDatabase admin -u ${user} -p ${password}

done

# Merge date & Time in ?????

echo "Processing Complete"


