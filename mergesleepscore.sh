#!/bin/bash

if [ $# -ne 3 ]
then
  echo "Usage: $0 <database> <username> <password> ..."
  exit 1
fi

db=$1
user=$2
password=$3

echo "Starting to process CSV files in ${PWD} into database ${db} as user ${user}"

for file in *.csv
do
    echo "Processing: ${file}"

    # Change header on first line
    sed -i '1 s|.*|_id.int64(),score_endTime.date(2006-01-02T15:04:05.000Z),score_overallScore.int32(),score_composition.int32(),score_revitalization.int32(),score_duration.int32(),score_deepSleepInMinutes.int32(),score_restingHeartRate.int32(),score_restlessness.double()|' ${file}

    # 06:27:00Z -> 06:27:00.000Z
    sed -i -r 's|([0-9]{2}):([0-9]{2}):([0-9]{2})Z|\1:\2:\3.000Z|g' ${file}

    collection=sleep
    echo "Loading Data into MongoDB Collection: ${collection}"

    # CSV Header
    # _id.int64(),score_endTime.date(2006-01-02T15:04:05.000Z),score_overallScore.int32(),score_composition.int32(),score_revitalization.int32(),score_duration.int32(),score_deepSleepInMinutes.int32(),score_restingHeartRate.int32(),score_restlessness.double()
    mongoimport --db ${db} --collection ${collection} --mode=merge --type=csv --headerline --columnsHaveTypes --file ${file} --authenticationDatabase admin -u ${user} -p ${password}

done

echo "Removing duplicate info loaded from CSV"

# score_deepSleepInMinutes
# score_endTime
# score_restingHeartRate ???

echo "Processing Complete"


